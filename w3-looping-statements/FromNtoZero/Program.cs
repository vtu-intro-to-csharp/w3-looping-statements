﻿using System;

namespace FromNtoZero
{
    class Program
    {
        //9.	Да се състави програма,
        //която приема като вход цяло положително число n.
        //Изведете всички числа от n до 0 (всяко на отдлен ред).
        static void Main(string[] args)
        {
            Console.Write("Enter an integer: ");
            int n = int.Parse(Console.ReadLine());

            for (int i = n; i >= 0; i--)
            {
                Console.WriteLine(i);
            }
        }
    }
}

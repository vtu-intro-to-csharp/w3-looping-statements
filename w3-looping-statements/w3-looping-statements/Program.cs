﻿using System;

namespace Combinations
{
    class Program
    {
        //16.	Да се състави програма,
        //която намира всички комбинации по двойки
        //на числата от 1 до 5 без повторение.
        static void Main(string[] args)
        {
            for (int i = 1; i <= 5; i++)
            {
                for (int j = i + 1; j <= 5; j++)
                {
                    Console.WriteLine(i + " " + j);
                }
            }
        }
    }
}

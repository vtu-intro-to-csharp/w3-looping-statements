﻿using System;

namespace SumOfNumbers
{
    class Program
    {
        //1.	Да се състави програма,
        //която очкава въвеждане на цели числа
        //до въвеждането на числото 0.
        //Намерете сумата на въведените числа.
        static void Main(string[] args)
        {
            Console.WriteLine("Enter integers. Use 0 to terminate.");
            int number;
            int sum = 0;
            do
            {
                Console.Write("Enter an integer: ");
                number = int.Parse(Console.ReadLine());
                sum += number; //sum = sum + number;
            } while (number != 0);

            Console.WriteLine("The sum of all numbers: " + sum);
        }
    }
}
